
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Tag("First")

public class MyMathTest {


    MyMath myMath=new MyMath();

    @Test
    @DisplayName("MyMath test on JUNIT5")
    void addTest(TestInfo testInfo){
        assertEquals(2,myMath.add(1,1),"1+1 should equal 2");
        assertEquals("My 1st JUnit 5 test  ",testInfo.getDisplayName(),()->"TestInfo is injected correctly");
    }

    @Test
    @DisplayName("MyMath test on JUNIT5")
    void divisionTest(TestInfo testInfo){
        assertEquals(2,myMath.division(4,2),"4/2 should equal 2");
        assertEquals("MyMath test on JUNIT5",testInfo.getDisplayName(),()->"TestInfo is injected correctly");
    }
}
