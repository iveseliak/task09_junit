import org.junit.Test;
import org.junit.jupiter.api.*;

public class FirstTest {

    public static String nameParam;
    public static int counter;

    @DisplayName("First test runs")

    @BeforeAll
    public void setUp(){
        nameParam="Hello world";
    }

    @Test
    @Disabled
    @Tag("counter")
    public void runTest(){
        for (int i=0; i<10;i++){
            counter=+i;
        }
    }

    @Test
    @Tag("double counter")
    public void runTest2(){
        for (int i=0; i<10;i++){
            counter=+i*2;
        }
    }

    @AfterAll
    public void tearDown(){

    }

    @AfterEach
    public void cleanUp(){
        counter=0;
    }


}
